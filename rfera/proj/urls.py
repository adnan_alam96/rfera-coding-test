"""
    rfera URL Configuration
    The `urlpatterns` list routes URLs to views.
"""

from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path("", include("frontend.urls")),
    path("", include("accounts.urls")),
    path("", include("devices.urls")),
    path("", include("events.urls")),
    path("admin/", admin.site.urls),
]
