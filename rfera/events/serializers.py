from rest_framework import serializers

from .models import Event


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ["id", "device_id", "raw_data", "status", "created_at"]

    device_id = serializers.SerializerMethodField("get_device_id")
    status = serializers.SerializerMethodField("get_status_text")

    def get_device_id(self, obj):
        return obj.device.device_id

    def get_status_text(self, obj):
        status_dict = {True: "Active", False: "Inactive"}
        status_val = obj.device.status
        return status_dict[status_val]
