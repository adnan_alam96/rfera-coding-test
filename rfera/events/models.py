from django.db import models

from devices.models import Device


class Event(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    raw_data = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-created_at",)

    def __str__(self):
        return self.device.name
