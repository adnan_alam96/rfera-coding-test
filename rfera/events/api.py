from rest_framework import viewsets, permissions

from devices.models import Device

from .models import Event
from .serializers import EventSerializer


# Event Viewset
class EventViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = EventSerializer

    def get_queryset(self):
        user = self.request.user
        devices = Device.objects.filter(user=user)
        events = Event.objects.filter(device__in=devices)
        return events
