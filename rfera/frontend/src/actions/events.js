import axios from "axios";
import { returnErrors } from "./messages";
import { tokenConfig } from "./auth";

import { GET_EVENTS } from "./types";

// GET EVENTS
export const getEvents = () => (dispatch, getState) => {
  axios
    .get("/api/events/", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_EVENTS,
        payload: res.data,
      });
    })
    .catch((err) =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};
