import axios from "axios";
import { createMessage, returnErrors } from "./messages";
import { tokenConfig } from "./auth";

import { GET_DEVICES, DELETE_DEVICE, ADD_DEVICE, UPDATE_DEVICE } from "./types";

// GET DEVICES
export const getDevices = () => (dispatch, getState) => {
  axios
    .get("/api/devices/", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_DEVICES,
        payload: res.data,
      });
    })
    .catch((err) =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

// DELETE DEVICE
export const deleteDevice = (id) => (dispatch, getState) => {
  axios
    .delete(`/api/devices/${id}/`, tokenConfig(getState))
    .then((res) => {
      dispatch(createMessage({ deleteDevice: "Device Deleted" }));
      dispatch({
        type: DELETE_DEVICE,
        payload: id,
      });
    })
    .catch((err) => console.log(err));
};

// ADD DEVICE
export const addDevice = (device) => (dispatch, getState) => {
  axios
    .post("/api/devices/", device, tokenConfig(getState))
    .then((res) => {
      dispatch(createMessage({ addDevice: "Device Added" }));
      dispatch({
        type: ADD_DEVICE,
        payload: res.data,
      });
    })
    .catch((err) =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

// UPDATE DEVICE
export const updateDevice = (id) => (dispatch, getState) => {
  axios
    .put(`/api/devices/${id}/`, tokenConfig(getState))
    .then((res) => {
      dispatch(createMessage({ updateDevice: "Device Updated" }));
      dispatch({
        type: UPDATE_DEVICE,
        payload: res.data,
      });
    })
    .catch((err) =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};
