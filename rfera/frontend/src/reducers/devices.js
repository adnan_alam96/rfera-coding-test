import {
  GET_DEVICES,
  DELETE_DEVICE,
  ADD_DEVICE,
  CLEAR_DEVICES,
  UPDATE_DEVICE,
} from "../actions/types.js";

const initialState = {
  devices: [],
  device_types: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_DEVICES:
      return {
        ...state,
        devices: action.payload.devices,
        device_types: action.payload.device_types,
      };
    case DELETE_DEVICE:
      return {
        ...state,
        devices: state.devices.filter((device) => device.id !== action.payload),
      };
    case ADD_DEVICE:
    case UPDATE_DEVICE:
      return {
        ...state,
        devices: [...state.devices, action.payload],
      };
    case CLEAR_DEVICES:
      return {
        ...state,
        devices: [],
      };
    default:
      return state;
  }
}
