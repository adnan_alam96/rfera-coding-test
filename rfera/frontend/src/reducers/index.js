import { combineReducers } from "redux";
import errors from "./errors";
import messages from "./messages";
import auth from "./auth";
import devices from "./devices";
import events from "./events";

export default combineReducers({
  errors,
  messages,
  auth,
  devices,
  events,
});
