import React, { Fragment } from "react";
import Events from "./Events";

export default function EventPage() {
  return (
    <Fragment>
      <Events />
    </Fragment>
  );
}
