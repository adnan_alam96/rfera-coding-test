import React, { Component, Fragment, forwardRef } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import store from "../../store";

import MaterialTable from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";

import { getEvents } from "../../actions/events";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

export class Events extends Component {
  static propTypes = {
    events: PropTypes.array.isRequired,
    getEvents: PropTypes.func.isRequired,
  };

  componentDidMount() {
    store.dispatch(getEvents());
  }

  render() {
    if (this.props.events === undefined || this.props.events.length === 0) {
      return (
        <Fragment>
          <div className="container">
            <div className="mt-5">
              <div className="col-lg-12">
                <div className="alert alert-info" role="alert">
                  No events to show.
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      );
    } else {
      const eventsData = [];
      this.props.events.map((event) =>
        eventsData.push({
          id: event.id,
          device_id: event.device_id,
          raw_data: event.raw_data,
          status: event.status,
          created_at: event.created_at,
        })
      );
      return (
        <Fragment>
          <div className="container">
            <div className="mt-5 mb-5">
              <MaterialTable
                icons={tableIcons}
                columns={[
                  { title: "Device ID", field: "device_id" },
                  { title: "Raw Data", field: "raw_data" },
                  { title: "Status", field: "status" },
                  { title: "Created At", field: "created_at" },
                ]}
                data={eventsData}
                title="Events"
                options={{
                  search: true,
                  filtering: true,
                  pageSize: 5,
                  pageSizeOptions: [5],
                }}
              />
            </div>
          </div>
        </Fragment>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  events: state.events.events,
});

export default connect(mapStateToProps, {
  getEvents,
})(Events);
