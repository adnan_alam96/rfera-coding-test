import React, { Component, Fragment, forwardRef } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import store from "../../store";

import MaterialTable from "material-table";

import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import IconButton from "@material-ui/core/IconButton";

import { getDevices, deleteDevice } from "../../actions/devices";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

export class Devices extends Component {
  static propTypes = {
    devices: PropTypes.array.isRequired,
    getDevices: PropTypes.func.isRequired,
    deleteDevice: PropTypes.func.isRequired,
  };

  render() {
    if (this.props.devices === undefined || this.props.devices.length === 0) {
      return (
        <Fragment>
          <div className="container">
            <div className="mt-5">
              <div className="col-lg-12">
                <div className="alert alert-info" role="alert">
                  No devices to show.
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      );
    } else {
      const devicesData = [];
      this.props.devices.map((device) =>
        devicesData.push({
          id: device.id,
          device_id: device.device_id,
          name: device.name,
          device_type: device.device_type,
          status: device.status,
        })
      );
      return (
        <Fragment>
          <div className="container">
            <div className="mt-5 mb-5">
              <MaterialTable
                icons={tableIcons}
                columns={[
                  { title: "Device ID", field: "device_id" },
                  { title: "Name", field: "name" },
                  { title: "Device Type", field: "device_type" },
                  { title: "Status", field: "status" },
                  {
                    title: "Delete",
                    render: (rowData) => (
                      <IconButton
                        color="secondary"
                        onClick={this.props.deleteDevice.bind(this, rowData.id)}
                      >
                        <DeleteOutline />
                      </IconButton>
                    ),
                  },
                ]}
                data={devicesData}
                title="Devices"
                options={{
                  search: true,
                  filtering: true,
                  pageSize: 5,
                  pageSizeOptions: [5],
                }}
              />
            </div>
          </div>
        </Fragment>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  devices: state.devices.devices,
});

export default connect(mapStateToProps, {
  getDevices,
  deleteDevice,
})(Devices);
