import React, { Fragment } from "react";
import Form from "./Form";
import Devices from "./Devices";

export default function DevicePage() {
  return (
    <Fragment>
      <Form />
      <Devices />
    </Fragment>
  );
}
