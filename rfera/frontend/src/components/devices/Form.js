import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import store from "../../store";
import { addDevice, getDevices } from "../../actions/devices";

export class Form extends Component {
  constructor() {
    super();
    this.state = {
      device_types: [],
      name: "",
      device_id: "",
      device_type: "",
      status: true,
    };
  }

  static propTypes = {
    addDevice: PropTypes.func.isRequired,
  };

  componentDidMount() {
    store.dispatch(getDevices());
    store.subscribe(() => {
      this.setState({
        device_types: store.getState().devices.device_types,
      });
    });
  }

  onChange = (e) => {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({ [target.name]: value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    const { name, device_id, device_type, status } = this.state;
    const device = { name, device_id, device_type, status };
    this.props.addDevice(device);
    this.setState({
      name: "",
      device_id: "",
      device_type: "",
      status: true,
    });
  };

  render() {
    const { name, device_id, status } = this.state;

    return (
      <div className="col-md-6 m-auto">
        <div className="card card-body mt-4 mb-4">
          <h2 className="text-center">Add Device</h2>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label>Device ID</label>
              <input
                className="form-control"
                type="text"
                name="device_id"
                placeholder="Enter Device ID (UUID)(e.g 9627d92c-8640-4f75-9cdb-11c837894daf)"
                onChange={this.onChange}
                value={device_id}
                required
              />
            </div>
            <div className="form-group">
              <label>Name</label>
              <input
                className="form-control"
                type="text"
                name="name"
                onChange={this.onChange}
                value={name}
                required
              />
            </div>
            <div className="form-group">
              <label htmlFor="deviceTypeId">Device Type</label>
              <select
                className="form-control"
                id="deviceTypeId"
                value={this.state.device_type}
                onChange={this.onChange}
                name="device_type"
                required
              >
                <option value="">Select Device Type</option>
                {this.state.device_types.map((device_type) => (
                  <option key={device_type.id} value={device_type.id}>
                    {device_type.value}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                id="device-status-id"
                type="checkbox"
                name="status"
                checked={status}
                onChange={this.onChange}
                value={status}
              />
              <label className="form-check-label" htmlFor="device-status-id">
                Status
              </label>
            </div>
            <div className="form-group">
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default connect(null, { addDevice })(Form);
