import uuid

from django.contrib.auth.models import User
from django.db import models

from .choices import DEVICE_TYPE_CHOICES


class Device(models.Model):
    user = models.ForeignKey(User, related_name="devices", on_delete=models.CASCADE)
    device_id = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=125)
    device_type = models.IntegerField(choices=DEVICE_TYPE_CHOICES, default=1)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("-created_at",)

    def __str__(self):
        return self.name
