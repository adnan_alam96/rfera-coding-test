from rest_framework import serializers

from .models import Device


class AddDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ["device_id", "name", "device_type", "status"]


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = "__all__"

    device_type = serializers.CharField(source="get_device_type_display")
    status = serializers.SerializerMethodField("get_status_text")

    def get_status_text(self, obj):
        status_dict = {True: "Active", False: "Inactive"}
        status_val = obj.status
        return status_dict[status_val]
