from rest_framework import routers

from .api import DeviceViewSet


router = routers.DefaultRouter()
router.register("api/devices", DeviceViewSet, "devices")

urlpatterns = router.urls
