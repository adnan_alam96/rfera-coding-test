from rest_framework import viewsets, permissions
from rest_framework.response import Response

from .choices import DEVICE_TYPE_CHOICES
from .serializers import DeviceSerializer, AddDeviceSerializer


# Device Viewset
class DeviceViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = AddDeviceSerializer

    def get_queryset(self):
        return self.request.user.devices.all()

    def list(self, request):
        devices = self.request.user.devices.all()
        serializer = DeviceSerializer(devices, many=True)
        devices_data = serializer.data
        device_types = dict(DEVICE_TYPE_CHOICES)
        device_types = [{"id": k, "value": v} for k, v in device_types.items()]
        return Response({"devices": devices_data, "device_types": device_types})

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
