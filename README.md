# RFERA

### Feaures

- Register, Login, Logout user
- Add, Delete, Read, Search, Filter Device
- Read, Search, Filter Event

### Installation

- Create virtual environment and activate. And then set this environment variable **SECRET_KEY**, .

- Install dependencies: `pip install -r requirements.txt` and `npm install` (from root directory).

- Go to **rfera** directory where the **manage.py** file exists. Make migrations: `./manage.py makemigrations` and then migrate: `./manage.py migrate`.

- Create superuser: `./manage.py createsuperuser`.

- Start the Django server with development settings: `./manage.py runserver --settings=proj.settings.development`

### Settings

- **settings/base.py** has the common settings of development and production.
- Development and production only settings are in the **settings/development.py** and **settings/production.py** respectively.

### Database

- sqlite3 database is used in this project.

### Run webpack (from root)

npm run dev

### Build for production

npm run build
